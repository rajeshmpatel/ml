function [J, grad] = costFunction(theta, X, y)
%COSTFUNCTION Compute cost and gradient for logistic regression
%   J = COSTFUNCTION(theta, X, y) computes the cost of using theta as the
%   parameter for logistic regression and the gradient of the cost
%   w.r.t. to the parameters.

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta
%
% Note: grad should have the same dimensions as theta
%

size(X);
size(theta);
size(y);

% h is 6x1  same as sigmoid
% y is also 6x1
% X is 6x3

tmp1 = - 1/m * (log(sigmoid(X * theta))' * y);
tmp2 = - 1/m * (log(1 - sigmoid(X * theta))' * (1-y));
J = tmp1 + tmp2;

%tmp1 = sigmoid(X * theta) - y
%tmp2 = X' * tmp1

grad = 1/m * (X' * (sigmoid(X * theta) - y)); % answer will be 3x1

% =============================================================

end
