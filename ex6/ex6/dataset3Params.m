function [C, sigma] = dataset3Params(X, y, Xval, yval)
%EX6PARAMS returns your choice of C and sigma for Part 3 of the exercise
%where you select the optimal (C, sigma) learning parameters to use for SVM
%with RBF kernel
%   [C, sigma] = EX6PARAMS(X, y, Xval, yval) returns your choice of C and 
%   sigma. You should complete this function to return the optimal C and 
%   sigma based on a cross-validation set.
%

% You need to return the following variables correctly.
C = 1;
sigma = 0.3;

% ====================== YOUR CODE HERE ======================
% Instructions: Fill in this function to return the optimal C and sigma
%               learning parameters found using the cross validation set.
%               You can use svmPredict to predict the labels on the cross
%               validation set. For example, 
%                   predictions = svmPredict(model, Xval);
%               will return the predictions on the cross validation set.
%
%  Note: You can compute the prediction error using 
%        mean(double(predictions ~= yval))
%

%%%%%
% Comments copied from the tutorial on how to look for best values of C and sigma
% https://www.coursera.org/learn/machine-learning/discussions/g2VB7po6EeWKNwpBrKr_Fw
% One method is to use two nested for-loops - each one iterating over the range of C or sigma values given in the ex6.pdf file.
% Inside the inner loop:
%
% Train the model using svmTrain with X, y, a value for C, and the gaussian kernel using a value for sigma.
% Compure the predictions for the validation set using svmPredict() with model and Xval.
% Compute the error between your predictions and yval.
% When you find a new minimum error, save the C and sigma values that were used.
%%%%%

mininfo = zeros(3,1); %store min error, corresponding C and sigma in a row
mininfo(1) = 1;
for i_C = [ 0.01, 0.03, 0.1, 0.3, 1.0, 3.0, 10.0, 30.0]
	for j_S = [ 0.01, 0.03, 0.1, 0.3, 1.0, 3.0, 10.0, 30.0]
		model = svmTrain(X, y, i_C, @(x1, x2) gaussianKernel(x1, x2, j_S));
		prediction = svmPredict(model, Xval);
		predError = mean(double(prediction ~= yval));
		% fprintf(['predError: %f, saved = %f, saved C = %f saved sigma = %f\n'], predError, mininfo);
		if(predError < mininfo(1))
			mininfo = [predError, i_C, j_S];
		end
	end
end

fprintf(['Final Prediction: C = %f, sigma = %f\n'], mininfo(2), mininfo(3));
C = mininfo(2);
sigma = mininfo(3);

% =========================================================================

end
